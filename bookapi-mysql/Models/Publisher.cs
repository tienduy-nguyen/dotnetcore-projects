using System.Collections.Generic;
namespace bookapi_mysql.Models {
  public class Publisher {
    public int Id { get; set; }
    public string Name { get; set; }
    public virtual ICollection<Book> Books { get; set; }
  }
}