using bookapi_mysql.Models;
using Microsoft.EntityFrameworkCore;
using MySQL.Data.EntityFrameworkCore;

namespace bookapi_mysql.Data {
  public class LibraryContext : DbContext {
    public LibraryContext (DbContextOptions<LibraryContext> options) : base (options) { }

    public DbSet<Book> Book { get; set; }
    public DbSet<Publisher> Publisher { get; set; }
    protected override void OnConfiguring (DbContextOptionsBuilder optionsBuilder) {
      // optionsBuilder.UseMySQL ("server=localhost;database=net_rpg_db;user=mysql;password=mysql;");
    }

    protected override void OnModelCreating (ModelBuilder modelBuilder) {
      base.OnModelCreating (modelBuilder);

      modelBuilder.Entity<Publisher> (entity => {
        entity.Property (e => e.Id).ValueGeneratedOnAdd ();
        entity.Property (e => e.Name).IsRequired ();
      });

      modelBuilder.Entity<Book> (entity => {
        entity.Property (e => e.Id).ValueGeneratedOnAdd ();
        entity.Property (e => e.ISBN);
        entity.Property (e => e.Title).IsRequired ();
        entity.HasOne (d => d.Publisher)
          .WithMany (p => p.Books);
      });
    }
  }
}