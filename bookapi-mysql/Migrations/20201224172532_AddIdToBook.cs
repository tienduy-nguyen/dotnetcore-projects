﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace bookapi_mysql.Migrations
{
    public partial class AddIdToBook : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Id",
                table: "Book",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Id",
                table: "Book");
        }
    }
}
