using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using todo_swagger.Models;

namespace todo_swagger {
  public class Startup {
    public Startup (IConfiguration configuration, IWebHostEnvironment env) {
      Environment = env;
      Configuration = configuration;
    }

    public IConfiguration Configuration { get; }
    public IWebHostEnvironment Environment { get; }

    // This method gets called by the runtime. Use this method to add services to the container.
    public void ConfigureServices (IServiceCollection services) {

      services.AddControllers ();

      services.AddDbContext<TodoContext> (options => {
        var connectionString = Configuration.GetConnectionString ("TodoContext");

        if (Environment.IsDevelopment ()) {
          options.UseSqlite (connectionString);
        } else {
          options.UseNpgsql (connectionString);
        }
      });

      // Register the Swagger generator, defining 1 or more Swagger documents
      services.AddSwaggerGen (c => {
        c.SwaggerDoc ("v1", new OpenApiInfo {
          Version = "v1",
            Title = "ToDo API",
            Description = "A simple example ASP.NET Core Web API",
            TermsOfService = new Uri ("https://example.com/terms"),
            Contact = new OpenApiContact {
              Name = "Shayne Boyer",
                Email = string.Empty,
                Url = new Uri ("https://twitter.com/spboyer"),
            },
            License = new OpenApiLicense {
              Name = "Use under LICX",
                Url = new Uri ("https://example.com/license"),
            }
        });

        // Set the comments path for the Swagger JSON and UI.
        var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
        var xmlPath = Path.Combine (AppContext.BaseDirectory, xmlFile);
        c.IncludeXmlComments (xmlPath);
      });
    }

    // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
    public void Configure (IApplicationBuilder app, IWebHostEnvironment env) {

      // Enable middleware to serve generated Swagger as a JSON endpoint.
      app.UseSwagger (); // version 3.0

      // old version 2.0
      // app.UseSwagger (c => {
      //   c.SerializeAsV2 = true;
      // });

      // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.),
      // specifying the Swagger JSON endpoint.
      app.UseSwaggerUI (c => {
        c.SwaggerEndpoint ("/swagger/v1/swagger.json", "My API V1");
        c.RoutePrefix = string.Empty;
      });

      if (env.IsDevelopment ()) {
        app.UseDeveloperExceptionPage ();
      }

      app.UseHttpsRedirection ();

      app.UseRouting ();

      app.UseAuthorization ();

      app.UseEndpoints (endpoints => {
        endpoints.MapControllers ();
      });
    }
  }
}