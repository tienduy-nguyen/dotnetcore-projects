using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using todo_swagger.Models;

namespace todo_swagger.Controllers {
  [Route ("api/[controller]")]
  [ApiController]
  public class TodosController : ControllerBase {
    private readonly TodoContext _context;

    public TodosController (TodoContext context) {
      _context = context;
    }

    /// <summary>
    /// Get all Todos Items in database
    /// </summary>
    /// <returns></returns>
    [HttpGet]
    public async Task<ActionResult<IEnumerable<TodoItemDTO>>> GetTodoItems () {
      return await _context.TodoItems
        .Select (x => ItemToDTO (x))
        .ToListAsync ();
    }

    /// <summary>
    /// Get a Todo Item by Id
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [HttpGet ("{id}")]
    public async Task<ActionResult<TodoItemDTO>> GetTodoItem (long id) {
      var todoItem = await _context.TodoItems.FindAsync (id);

      if (todoItem == null) {
        return NotFound ();
      }

      return ItemToDTO (todoItem);
    }

    /// <summary>
    /// Update a specific Todo Item
    /// </summary>
    /// <param name="id"></param>
    /// <param name="todoItemDTO"></param>
    /// <returns></returns>
    [HttpPut ("{id}")]
    public async Task<IActionResult> PutTodoItem (long id, TodoItemDTO todoItemDTO) {
      if (id != todoItemDTO.Id) {
        return BadRequest ();
      }

      var todoItem = await _context.TodoItems.FindAsync (id);
      if (todoItem == null) {
        return NotFound ();
      }

      todoItem.Name = todoItemDTO.Name;
      todoItem.IsComplete = todoItemDTO.IsComplete;

      try {
        await _context.SaveChangesAsync ();
      } catch (DbUpdateConcurrencyException) when (!TodoItemExists (id)) {
        return NotFound ();
      }

      return NoContent ();
    }

    /// <summary>
    /// Creates a TodoItem.
    /// </summary>
    /// <remarks>
    /// Sample request:
    ///
    ///     POST /todos
    ///     {
    ///        "id": 1,
    ///        "name": "Item1",
    ///        "isComplete": true
    ///     }
    ///
    /// </remarks>
    /// <param name="item"></param>
    /// <returns>A newly created TodoItem</returns>
    /// <response code="201">Returns the newly created item</response>
    /// <response code="400">If the item is null</response> 
    [HttpPost]
    [ProducesResponseType (StatusCodes.Status201Created)]
    [ProducesResponseType (StatusCodes.Status400BadRequest)]
    public async Task<ActionResult<TodoItemDTO>> PostTodoItem (TodoItemDTO todoItemDTO) {
      var todoItem = new TodoItem {
        IsComplete = todoItemDTO.IsComplete,
        Name = todoItemDTO.Name
      };
      _context.TodoItems.Add (todoItem);
      await _context.SaveChangesAsync ();

      return CreatedAtAction ("GetTodoItem", new { id = todoItem.Id }, ItemToDTO (todoItem));
    }

    /// <summary>
    /// Delete a specific Todo Item
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [HttpDelete ("{id}")]
    public async Task<ActionResult<TodoItem>> DeleteTodoItem (long id) {
      var todoItem = await _context.TodoItems.FindAsync (id);
      if (todoItem == null) {
        return NotFound ();
      }

      _context.TodoItems.Remove (todoItem);
      await _context.SaveChangesAsync ();

      return NoContent ();
    }

    private bool TodoItemExists (long id) {
      return _context.TodoItems.Any (e => e.Id == id);
    }

    // Convert TodoItem to TodoItemDTO
    private static TodoItemDTO ItemToDTO (TodoItem todoItem) =>
      new TodoItemDTO {
        Id = todoItem.Id,
        Name = todoItem.Name,
        IsComplete = todoItem.IsComplete
      };
  }
}