using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using todo_nswag.Models;

namespace todo_nswag {
  public class Startup {
    public Startup (IConfiguration configuration, IWebHostEnvironment env) {
      Environment = env;
      Configuration = configuration;
    }

    public IConfiguration Configuration { get; }
    public IWebHostEnvironment Environment { get; }

    // This method gets called by the runtime. Use this method to add services to the container.
    public void ConfigureServices (IServiceCollection services) {

      services.AddControllers ();

      services.AddDbContext<TodoContext> (options => {
        var connectionString = Configuration.GetConnectionString ("TodoContext");

        if (Environment.IsDevelopment ()) {
          options.UseSqlite (connectionString);
        } else {
          options.UseNpgsql (connectionString);
        }
      });

      // Register the Swagger services
      services.AddSwaggerDocument ();
    }

    // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
    public void Configure (IApplicationBuilder app, IWebHostEnvironment env) {

      // Register the Swagger generator and the Swagger UI middlewares
      app.UseOpenApi ();
      app.UseSwaggerUi3 ();
      if (env.IsDevelopment ()) {
        app.UseDeveloperExceptionPage ();
      }

      app.UseHttpsRedirection ();

      app.UseRouting ();

      app.UseAuthorization ();

      app.UseEndpoints (endpoints => {
        endpoints.MapControllers ();
      });
    }
  }
}