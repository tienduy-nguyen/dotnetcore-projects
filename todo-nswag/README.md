# Todo api .net core

- [Todo api .net core](#todo-api-net-core)
  - [Requires](#requires)
  - [Installation](#installation)
  - [Development](#development)
    - [Config site url](#config-site-url)
    - [Models](#models)
    - [Configuration database](#configuration-database)
    - [Update `databasse` in `Startup.cs`](#update-databasse-in-startupcs)
    - [Scaffold TodosController](#scaffold-todoscontroller)
    - [Migrations](#migrations)
    - [Test the app](#test-the-app)
  - [Swagger /OpenAPi](#swagger-openapi)
    - [Add and configure Swagger middleware](#add-and-configure-swagger-middleware)
    - [Customize and extend](#customize-and-extend)
  - [Reference](#reference)
## Requires

- Dotnet version 3.1.302


## Installation


Add packages indispensables:

```s
dotnet add package Microsoft.AspNetCore.Diagnostics.EntityFrameworkCore --version 3.1
dotnet add package Microsoft.EntityFrameworkCore.Design --version 3.1
dotnet add package Microsoft.EntityFrameworkCore.Sqlite --version 3.1
dotnet add package Microsoft.Extensions.Logging.Debug --version 3.1
dotnet add package Microsoft.VisualStudio.Web.CodeGeneration.Design --version 3.1
dotnet add package Npgsql.EntityFrameworkCore.PostgreSQL --version 3.1
dotnet add TodoApi.csproj package Swashbuckle.AspNetCore
```

## Development

### Config site url

Edit in `Properties/launchSettings.json` at `applicationUrl` of todo_api:

```json
...
"applicationUrl": "http://localhost:5001",
...
```
### Models

- Create `Models` folders in root project
- Create `Models/TodoItem.cs`: Class for todo item
  ```cs
  // Models/TodoItems.cs
  namespace todo_api.Models {
    public class TodoItem {
      public long Id { get; set; }
      public string Name { get; set; }
      public bool IsComplete { get; set; }
      public string Secret { get; set; }
    }
  }
  ```

- Create `Models/TodoContext.cs`: Context database for todo item
  ```cs
  using Microsoft.EntityFrameworkCore;

  namespace todo_api.Models {
    public class TodoContext : DbContext {
      public TodoContext (DbContextOptions<TodoContext> options) : base (options) { }

      public DbSet<TodoItem> TodoItems { get; set; }
    }
  }
  ```
- Create `Models/TodoItemDTO.cs`
  
  Data transfer object: This class using to transfer data between form and object. It will help us to hide the secret field of todo item.

  ```cs
  namespace todo_api.Models {
  public class TodoItemDTO {

      public long Id { get; set; }
      public string Name { get; set; }
      public bool IsComplete { get; set; }
    }
  }
  ```
### Configuration database

In this demo, we will use SQLite for dev and PostgreQL for production.

Edit in `appSettings.json` or create `appSettings.Production.json`

```json
{
  "Logging": {
    "LogLevel": {
      "Default": "Information",
      "Microsoft": "Warning",
      "Microsoft.Hosting.Lifetime": "Information"
    }
  },
  "AllowedHosts": "*",
  "ConnectionStrings": {
    "TodoContext": "conection string of database postgreSQL  "
  }
}
```

If you want to use SQLite for development, edit in `appSettings.Development.json`

```json
{
  "Logging": {
    "LogLevel": {
      "Default": "Information",
      "Microsoft": "Warning",
      "Microsoft.Hosting.Lifetime": "Information"
    }
  },
  "ConnectionStrings": {
    "TodoContext": "Data Source=Databases/Todo.db"
  }
}
```

### Update `databasse` in `Startup.cs`

```cs
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using todo_api.Models;

namespace todo_api {
  public class Startup {
    public Startup (IConfiguration configuration, IWebHostEnvironment env) {
      Environment = env;
      Configuration = configuration;
    }

    public IConfiguration Configuration { get; }
    public IWebHostEnvironment Environment { get; }

    // This method gets called by the runtime. Use this method to add services to the container.
    public void ConfigureServices (IServiceCollection services) {

      services.AddControllers ();
      // Integrate DB context to services
      services.AddDbContext<TodoContext> (options => {
        var connectionString = Configuration.GetConnectionString ("TodoContext");

        if (Environment.IsDevelopment ()) {
          options.UseSqlite (connectionString);
        } else {
          options.UseNpgsql (connectionString);
        }
      });
    }

    // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
    public void Configure (IApplicationBuilder app, IWebHostEnvironment env) {
      if (env.IsDevelopment ()) {
        app.UseDeveloperExceptionPage ();
      }

      app.UseHttpsRedirection ();

      app.UseRouting ();

      app.UseAuthorization ();

      app.UseEndpoints (endpoints => {
        endpoints.MapControllers ();
      });
    }
  }
}
```

### Scaffold TodosController

Make sure your have `dotnet-aspnet-codegenerator` in your local.

```s
$ dotnet tool install --global dotnet-aspnet-codegenerator --version 3.1.3
```

Scaffold TodosController:
```s
$ dotnet aspnet-codegenerator controller -name TodosController -async -api -m TododItem -dc TodoContext -outDir Controllers
```

When you run the command above, it will help you gererate `TodosController.cs` in `Controllers` folder with all method of CRUD.


Now, you need modify a little bit to use `TodoItemDTO`:

```cs
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using todo_api.Models;

namespace todo_api.Controllers {
  [Route ("api/[controller]")]
  [ApiController]
  public class TodosController : ControllerBase {
    private readonly TodoContext _context;

    public TodosController (TodoContext context) {
      _context = context;
    }

    // GET: api/Todos
    [HttpGet]
    public async Task<ActionResult<IEnumerable<TodoItemDTO>>> GetTodoItems () {
      return await _context.TodoItems
        .Select (x => ItemToDTO (x))
        .ToListAsync ();
    }

    // GET: api/Todos/5
    [HttpGet ("{id}")]
    public async Task<ActionResult<TodoItemDTO>> GetTodoItem (long id) {
      var todoItem = await _context.TodoItems.FindAsync (id);

      if (todoItem == null) {
        return NotFound ();
      }

      return ItemToDTO (todoItem);
    }

    // PUT: api/Todos/5
    // To protect from overposting attacks, please enable the specific properties you want to bind to, for
    // more details see https://aka.ms/RazorPagesCRUD.
    [HttpPut ("{id}")]
    public async Task<IActionResult> PutTodoItem (long id, TodoItemDTO todoItemDTO) {
      if (id != todoItemDTO.Id) {
        return BadRequest ();
      }

      var todoItem = await _context.TodoItems.FindAsync (id);
      if (todoItem == null) {
        return NotFound ();
      }

      todoItem.Name = todoItemDTO.Name;
      todoItem.IsComplete = todoItemDTO.IsComplete;

      try {
        await _context.SaveChangesAsync ();
      } catch (DbUpdateConcurrencyException) when (!TodoItemExists (id)) {
        return NotFound ();
      }

      return NoContent ();
    }

    // POST: api/Todos
    // To protect from overposting attacks, please enable the specific properties you want to bind to, for
    // more details see https://aka.ms/RazorPagesCRUD.
    [HttpPost]
    public async Task<ActionResult<TodoItemDTO>> PostTodoItem (TodoItemDTO todoItemDTO) {
      var todoItem = new TodoItem {
        IsComplete = todoItemDTO.IsComplete,
        Name = todoItemDTO.Name
      };
      _context.TodoItems.Add (todoItem);
      await _context.SaveChangesAsync ();

      return CreatedAtAction ("GetTodoItem", new { id = todoItem.Id }, ItemToDTO (todoItem));
    }

    // DELETE: api/Todos/5
    [HttpDelete ("{id}")]
    public async Task<ActionResult<TodoItem>> DeleteTodoItem (long id) {
      var todoItem = await _context.TodoItems.FindAsync (id);
      if (todoItem == null) {
        return NotFound ();
      }

      _context.TodoItems.Remove (todoItem);
      await _context.SaveChangesAsync ();

      return NoContent ();
    }

    private bool TodoItemExists (long id) {
      return _context.TodoItems.Any (e => e.Id == id);
    }

    // Convert TodoItem to TodoItemDTO
    private static TodoItemDTO ItemToDTO (TodoItem todoItem) =>
      new TodoItemDTO {
        Id = todoItem.Id,
        Name = todoItem.Name,
        IsComplete = todoItem.IsComplete
      };
  }
}
```
### Migrations

OK, if you arrive this step, it means all codes of your project almost done.

The thing you need to do rightnow is migration.

The ORM database need to migration to use.

- Initial migration
  To migrate your class models to database, you will use a tool called : `dotnet-ef`

  If `dotnet-ef` has not been installed, install it as a global tool:
  ```s
  $ dotnet tool install --global dotnet-ef
  ```

  Run the following .NET Core CLI commands:

  ```s
  $ dotnet ef migrations add InitialCreate
  $ dotnet ef database update
  ```

### Test the app

Ok, now it's time to test project:

Run project with command: `dotnet run` or `dotnet watch run` to update automatically the modification of project.

Check results at: `https://localhost:5001/todos`

To use CRUD, you can use `postman` to test all results.


## Swagger /OpenAPi

Start with Swashbuckle package.
### Add and configure Swagger middleware

- Edit in `Startup.ConfigureServices`:

  Make sure you have installed package `Swashbuckle.AspNetCore`

  ```s
  $ dotnet add package Swashbuckle.AspNetCore
  ```

  ```cs
  //Startup.cs
  public void ConfigureServices (IServiceCollection services) {

        services.AddControllers ();

        services.AddDbContext<TodoContext> (options => {
          var connectionString = Configuration.GetConnectionString ("TodoContext");

          if (Environment.IsDevelopment ()) {
            options.UseSqlite (connectionString);
          } else {
            options.UseNpgsql (connectionString);
          }
        });

        // Register the Swagger generator, defining 1 or more Swagger documents
        services.AddSwaggerGen ();
      }
  ```
- Update Swagger in `Startup.Configure`
  ```cs
    // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
    public void Configure (IApplicationBuilder app, IWebHostEnvironment env) {

      // Enable middleware to serve generated Swagger as a JSON endpoint.
      app.UseSwagger ();

      // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.),
      // specifying the Swagger JSON endpoint.
      app.UseSwaggerUI (c => {
        c.SwaggerEndpoint ("/swagger/v1/swagger.json", "My API V1");
        c.RoutePrefix = string.Empty;
      });

      if (env.IsDevelopment ()) {
        app.UseDeveloperExceptionPage ();
      }

      app.UseHttpsRedirection ();

      app.UseRouting ();

      app.UseAuthorization ();

      app.UseEndpoints (endpoints => {
        endpoints.MapControllers ();
      });
    }
  ```
### Customize and extend

Check detail on [docs.mircrosoft/Swagger-OpenApi](https://docs.microsoft.com/en-us/aspnet/core/tutorials/getting-started-with-swashbuckle?view=aspnetcore-3.1&tabs=visual-studio-code)

## Reference

- [Microsoft tuto: Create a web API](https://docs.microsoft.com/en-us/aspnet/core/tutorials/first-web-api?view=aspnetcore-3.1&tabs=visual-studio-code)